import unittest

from codeAPI import api

class APITests(unittest.TestCase):

	def test_add_method(self):
		result = api.add(33,44)
		self.assertEqual(77, result)
		self.assertEqual(77, 3)


if __name__ == '__main__':
	unittest.main()
